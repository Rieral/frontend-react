import React from 'react'

// import axios from "axios";
// import { useEffect, useState } from "react";
// import { useNavigate, useParams } from "react-router-dom";

// const URL = 'http://localhost:9000'

const FormularioEditarFamilia = () => {

    // const [title, setTitle] = useState('')    
    // const [content, setContent] = useState('')    
    // const navigate = useNavigate()
    // const {id} = useParams()

    // //procedimiento para actualizar
    // const update = async (e) => {
    //     e.preventDefault()
    //     await axios.put(URI+id, {
    //         title: title,
    //         content: content
    //     })
    //     navigate('/tablefamily')
    // }

    // useEffect( ()=>{
    //     getFamilyById()
    // },[])

    // const getFamilyById = async () => {
    //     const res = await axios.get(URL+id)
    //     setTitle(res.data.title)
    //     setContent(res.data.content)
    // }

  return (
    <div className='container'>
        <h3 className='text-center'>Formulario Editar familia</h3>
        <hr/>
        <div className='row d-flex justify-content-center'>
            <form className='col-6'>
                <fieldset disabled>
                    <div className="mb-3">
                    <label for="disabledTextInput" className="form-label">Nombre</label>
                    <input type="text" id="disabledTextInput" className="form-control" placeholder="Disabled input" />
                    </div>
                    <div className="mb-3">
                    <label for="disabledTextInput" className="form-label">Codigo</label>
                    <input type="text" id="disabledTextInput" className="form-control" placeholder="Disabled input" />
                    </div>
                    <div className="mb-3">
                    <label for="disabledTextInput" className="form-label">orden</label>
                    <input type="text" id="disabledTextInput" className="form-control" placeholder="Disabled input" />
                    </div>
                    <div className="mb-3">
                    <label for="disabledSelect" className="form-label">Familia</label>
                    <select id="disabledSelect" className="form-select">
                        <option>Disabled select</option>
                    </select>
                    </div>
                    <div className="mb-3">
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" id="disabledFieldsetCheck" disabled />
                        <label className="form-check-label" for="disabledFieldsetCheck">
                        Informar
                        </label>
                    </div>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    </div>
  )
}

export default FormularioEditarFamilia