import React, {useState, useEffect} from 'react'


// import {Link} from 'react-router-dom'
// import axios from 'axios'

// const URL = 'http://localhost:9000/'


import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPenToSquare ,faTrash } from '@fortawesome/free-solid-svg-icons'

const TablaFamilia  = () => {
  const [datos, setDatos] = useState([]);

  const datosFamilia = [
    {
        id:1,
        name: "Antiviotico 2",
        code: "code 1",
        familia:"dasdas"

    },
    {
        id:2,
        name: "Antiviotico 1",
        code: "code 2",
        familia:"dasdas"

    },

  ]

  // const [familia, setFamilia] = useState([])
  // useEffect( ()=>{
  //     getFamilia()
  // },[])

  //procedimineto para mostrar todos los blogs
  // const getFamilia = async () => {
  //     const res = await axios.get(URL)
  //     setFamilia(res.data)
  // }

  // //procedimineto para eliminar un blog
  // const deleteFamilia = async (id) => {
  //    await axios.delete(`${URI}${id}`)
  //    getFamilia()
  // }

  return (
    <div className='container'>
     
        <h3 className='text-center'>Tablas Familia</h3>
        <hr/>
        <div className='row text.center'>
        <div className='col-6 '>
        <Link className='btn btn-primary' data-bs-toggle="modal" to='/form'>Agregar antibiotico</Link>
        </div> 
        <div className='col-6 '>
        <Link className='btn btn-success mx-2' data-bs-toggle="modal" to='/form'>Tabla familia</Link>
        <Link className='btn btn-success ' data-bs-toggle="modal" to='/formfamily'>Agregar familia</Link>
        </div>   
        
          
        </div>
        <br/>
        <div className='row'>
          <table className="table">
          <thead>
              <tr>
              <th scope="col">id</th>
              <th scope="col">Nombre</th>
              <th scope="col">Codigo</th>
              <th scope="col ml-5">Familia </th>
              <th scope="col">Acciones</th>
              </tr>
          </thead>
          <tbody>
              
                {
                datosFamilia.map((dato)=>{
                  return(
                    <>
                    <tr>
                    <th scope="row">{dato.id}</th>
                      <td>{dato.name}</td>
                      <td>{dato.code}</td>
                      <td>{dato.familia}</td>

                      <td><Link to={'/formeditfamily'} type="button" className="btn btn-primary "> <FontAwesomeIcon icon={faPenToSquare} /></Link>  
                      <button type="button" className="btn btn-danger"><FontAwesomeIcon icon={faTrash} /></button></td>

                    </tr>
                      
                    </>
                    
                  )
                  
                })}
          </tbody>
          </table>
        </div>        
        

    </div>
    
  )
}

export default TablaFamilia