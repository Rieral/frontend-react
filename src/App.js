import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

// Antibioticos
import Tabla from './components/antibioticos/Tabla';
import Formulario from './components/antibioticos/Formulario';
import FormularioEditar from './components/antibioticos/Formularioeditar';

//Familia
import TablaFamilia from './components/familia/TablaFamilia';
import FormularioFamilia from './components/familia/FormularioFamilia';
import FormularioEditarFamilia from './components/familia/FormularioEditarFamilia';

function App() {
  return (
    <>
      <h1 className='text-center'>Antibiotico</h1>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Tabla/>}/>
          <Route path='/form' element={<Formulario/>}/>
          <Route path='/edit' element={<FormularioEditar/>}/>

          <Route path='/tablefamily' element={<TablaFamilia/>}/>
          <Route path='/formfamily' element={<FormularioFamilia/>}/>
          <Route path='/formeditfamily' element={<FormularioEditarFamilia/>}/>
          
          
          
        </Routes>
      </BrowserRouter>
      
      
    </>
  );
}

export default App;
